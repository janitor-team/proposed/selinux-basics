class TestPAMConfig(TestBase):
	"""
	Verify that pam is setup for SELinux
	"""
	class ErrorPAMSSH(ErrorBase):
		def __str__(self):
			return "/etc/pam.d/sshd is not SELinux enabled"

	class ErrorPAMLogin(ErrorBase):
		def __str__(self):
			return "/etc/pam.d/login is not SELinux enabled"

	@staticmethod
	def test():
		import os, re
		r = re.compile(r'^\s*session\s+(required|\[success=ok\s+ignore=ignore\s+module_unknown=ignore\s+default=bad\])\s+pam_selinux.so')
		result = []

		if os.access("/etc/pam.d/sshd", os.F_OK):
			selinuxon = False
			f = open("/etc/pam.d/sshd","r")
			for line in f.readlines():
				if r.match(line):
					selinuxon = True
			f.close()
			if not selinuxon:
				result.append(TestPAMConfig.ErrorPAMSSH())

		if os.access("/etc/pam.d/login", os.F_OK):
			selinuxon = False
			f = open("/etc/pam.d/login","r")
			for line in f.readlines():
				if r.match(line):
					selinuxon = True
			f.close()
			if not selinuxon:
				result.append(TestPAMConfig.ErrorPAMLogin())
		return result
register_test(TestPAMConfig)
