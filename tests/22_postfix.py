class TestPostfixChroot(TestBase):
	"""
	Verify that postfix is not using chroots.
	"""
	class ErrorPostfixChrootSync(ErrorBase):
		def __str__(self):
			return "Postfix init script is syncing the chroots."
		def fixable(self):
			return False
		def fix(self):
			return False

	class ErrorPostfixChroot(ErrorBase):
		def __str__(self):
			return "Postfix has chrooted service in master.cf"
		def fixable(self):
			return True
		def fix(self):
			return not TestPostfixChroot.process_mastercf(fix=True)

	@staticmethod
	def process_mastercf(fix=False):
		import re
		has_chroot = False
		split = re.compile("([^\s]+)")
		f = open("/etc/postfix/master.cf", "r")
		if fix:
			fixed = open("/etc/postfix/master.cf.fixed", "w")
		for line in f.readlines():
			# ignore lines with leading whitespace (extra options) and comments
			if line[0].isspace() or line[0] == '#':
				if fix: fixed.write(line)
				continue
			col = split.split(line)
			# primitive check we "understand" this line...
			if col[3] in ["unix","inet","fifo"] \
			and col[5] in ["-", "y", "n"] \
			and col[7] in ["-", "y", "n"] \
			and col[9] in ["-", "y"]:
				has_chroot = True
				if fix:
					col[9] = 'n'
					fixed.write("".join(col))
			else:
				if fix: fixed.write(line)
		f.close()
		if fix:
			fixed.close()
		return has_chroot

	@staticmethod
	def test():
		import os, re
		r = re.compile(r'^\s*SYNC_CHROOT=(.*n|\s*$|\s*""$)')
		result = []

		if os.access("/usr/sbin/postfix", os.F_OK):
			sync = True
			if os.access("/etc/default/postfix", os.F_OK):
				f = open("/etc/default/postfix","r")
				for line in f.readlines():
					if r.match(line):
						sync = False
				f.close()
			if sync:
				result.append(TestPostfixChroot.ErrorPostfixChrootSync())

		if os.access("/etc/postfix/master.cf", os.F_OK):
			if TestPostfixChroot.process_mastercf(fix=False):
				result.append(TestPostfixChroot.ErrorPostfixChroot())
		return result
register_test(TestPostfixChroot)
