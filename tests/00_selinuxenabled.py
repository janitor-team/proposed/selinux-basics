class TestSELinuxEnabled(TestBase):
	"""
	Test that selinux is enabled, by calling selinuxenabled
	"""
	class ErrorNotEnabled(ErrorBase):
		def __str__(self):
			return "SELinux is not enabled."

	class ErrorCouldNotTest(ErrorBase):
		def __str__(self):
			return "Couldn't test if selinux is enabled."

	@staticmethod
	def test():
		import os
		status = os.system("selinuxenabled")
		if not os.WIFEXITED(status):
			return [TestSELinuxEnabled.ErrorCouldNotTest()]

		exitcode = os.WEXITSTATUS(status)
		if exitcode != 0:
			return [TestSELinuxEnabled.ErrorNotEnabled()]
		return []

register_test(TestSELinuxEnabled)
