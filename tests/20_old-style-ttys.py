class TestOldTTYs(TestBase):
	"""
	Check for old-style ttys that are not supported by the policies.
	These are disabled in the default kernel shipped by Debian since
	Squeeze.
	"""

	class ErrorOldTTYs(ErrorBase):
		def __init__(self, oldttys):
			self.oldttys = oldttys
		def __str__(self):
			return "Old style ttys were found."
		def fixable(self):
			return True
		def fix(self):
			success = True
			for nam in self.oldttys:
				if not os.unlink(nam):
					success = False
			return success

	@staticmethod
	def test():
		import glob

		oldttys = glob.glob("/dev/[tp]ty[abcdepqrstuvwxyz][0-9a-f]")
		if len(oldttys) > 0:
			return [TestOldTTYs.ErrorOldTTYs(oldttys)]
		return []
register_test(TestOldTTYs)
