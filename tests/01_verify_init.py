class TestInitDomain(TestBase):
	"""
	Test that pid 1 (init) is running in the appropriate domain
	"""
	class ErrorGetfileconFailed(ErrorBase):
		def __str__(self):
			return "Could not read the domain of PID 1."

	class ErrorInitBadlyLabeled(ErrorBase):
		def __str__(self):
			return "The init process (PID 1) is running in an incorrect domain."

	@staticmethod
	def test():
		from subprocess import Popen, PIPE

		contextok = False

		pipe = Popen("getfilecon /proc/1", shell=True, stdin=PIPE, stdout=PIPE, stderr=PIPE, close_fds=True, universal_newlines=True)
		pipe.stdin.close()

		for line in pipe.stdout.readlines():
			line = line.rstrip()
			if line == "": continue
			if line.endswith(":system_r:init_t") \
				or line.find(":system_r:init_t:") >= 0:
				contextok = True
			else:
				print("..%s.." % line)
		pipe.stdout.close()

		for line in pipe.stderr.readlines():
			if line.find("failed") >= 0:
				contextok = "failed"
		pipe.stderr.close()

		if contextok == "failed":
			return [TestInitDomain.ErrorGetfileconFailed()]
		if not contextok:
			return [TestInitDomain.ErrorInitBadlyLabeled()]
		return []
register_test(TestInitDomain)
